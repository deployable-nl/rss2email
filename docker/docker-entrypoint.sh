#!/bin/sh

if [ ! -d ~/.config ]; then
    mkdir ~/.config;
fi

if [ "$1" = "run" ]; then
    while :
    do
        envsubst < /etc/r2e/rss2email.cfg.dist > ~/.config/rss2email.cfg
        echo "Parsing the feeds"
        if [ -f ~/.local/share/rss2email.json ]; then 
            r2e run; 
        else 
            echo "First run, not sending e-mails"
            r2e run --no-send; 
        fi  
        echo "Going to sleep for ${R2E_RUN_INTERVAL} seconds"
        sleep "${R2E_RUN_INTERVAL:-300}";
    done
fi

exec "$@" 
